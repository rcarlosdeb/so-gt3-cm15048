.section .data
num1: .long 0
num2: .long 0
introValor1: .ascii "Introducir sumando 1\0",
introValor2: .ascii "Introducir sumando 2\0",
resultado: .ascii "El resultado es: %d\n"
tipo: .ascii "%d"

.section .bss

.section .text

.globl main

main:
	pushl	$introValor1
	call	printf
	addl	$4,	%esp
	pushl	$num1
	pushl	$tipo
	call	scanf
	addl	$4,	%esp

	pushl	$introValor2
	call	printf
	addl	$4,	%esp
	pushl	$num2
	pushl	$tipo
	call	scanf
	addl	$8,	%esp

	movl	num1,	%eax
	addl	num2,	%eax
	pushl		%eax
	pushl	$resultado
	call	printf
	addl	$4,	%esp
	movl	$0,	%ebx
	movl	$1,	%eax
	int	$0x80
